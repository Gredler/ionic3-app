import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

export class Alerts {
  public static alert(
    title: string,
    message: string,
    alertCtrl: AlertController
  ) {
    alertCtrl
      .create({
        title: title,
        subTitle: message,
        buttons: ['Ok']
      })
      .present()
      .catch(e => console.error(e));
  }

  public static alertWithPop(
    title: string,
    message: string,
    alertCtrl: AlertController,
    navCtrl: NavController
  ) {
    alertCtrl
      .create({
        title: title,
        subTitle: message,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              navCtrl.pop().catch(e => console.log(e));
            }
          }
        ]
      })
      .present()
      .catch(e => console.error(e));
  }

  public static logoutAlert(
    title: string,
    message: string,
    alertCtrl: AlertController,
    navCtrl: NavController,
    fire: AngularFireAuth,
    storage: Storage
  ) {
    alertCtrl
      .create({
        title: title,
        subTitle: message,
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              // logout
              fire.auth.signOut().catch(e => console.log(e));
              storage.clear().catch(e => console.log(e));
              navCtrl.pop().catch(e => console.log(e));
            }
          },
          {
            // don't
            text: 'No'
          }
        ]
      })
      .present()
      .catch(e => console.error(e));
  }
}
