export enum EAuthProviders {
  facebook = 0,
  google,
  twitter,
  github,
  local
}
