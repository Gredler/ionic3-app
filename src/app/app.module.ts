import { ChangePasswordPage } from './../pages/change-password/change-password';
import { OptionsPage } from './../pages/options/options';
import { AboutPage } from './../pages/about/about';
import { UserLoginPage } from './../pages/user-login/user-login';
import { LoginPage } from './../pages/login/login';
import { TabsPage } from './../pages/tabs/tabs';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';

import { Camera } from '@ionic-native/camera';

var config = {
  apiKey: "AIzaSyAcx_nz5NIF03M7Uw_q3WJv5VUi8tM9kAQ",
  authDomain: "test-project-54f8d.firebaseapp.com",
  databaseURL: "https://test-project-54f8d.firebaseio.com",
  projectId: "test-project-54f8d",
  storageBucket: "test-project-54f8d.appspot.com",
  messagingSenderId: "497263715922"
};

// Manifest writer issue fix (do it manually ...):
// https://github.com/walteram/cordova-universal-links-plugin/commit/b2c5784764225319648e26aa5d3f42ede6d1b289

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    UserLoginPage,
    RegisterPage,
    AboutPage,
    OptionsPage,
    ChangePasswordPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {tabsHideOnSubPages: true}),
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    UserLoginPage,
    RegisterPage,
    AboutPage,
    OptionsPage,
    ChangePasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
  ]
})
export class AppModule {}
