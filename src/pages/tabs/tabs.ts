import { OptionsPage } from './../options/options';
import { NavController, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

// import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
import { Alerts } from '../../alerts';
import { AboutPage } from '../about/about';
import { User } from '../../User';
import { EAuthProviders } from '../../EAuth';
// import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = HomePage;
  tab3Root = AboutPage;
  tab4Root = OptionsPage;

  loggedIn: boolean = false;

  providers = EAuthProviders;

  user: User = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    private navCtrl: NavController,
    private fire: AngularFireAuth,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {
    this.fire.authState.subscribe(res => {
      if (res && res.uid && !res.isAnonymous) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });

    this.storage
      .get('user')
      .then(val => {
        this.user = JSON.parse(val);
      })
      .catch(e => console.error(e));
  }

  public popPage() {
    this.navCtrl.pop().catch(e => console.error(e));
  }

  logout() {
    Alerts.logoutAlert(
      'Logout',
      'Do You really want to log out?',
      this.alertCtrl,
      this.navCtrl,
      this.fire,
      this.storage
    );
  }
}
