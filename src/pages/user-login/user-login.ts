import { TabsPage } from './../tabs/tabs';
import { RegisterPage } from './../register/register';
import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { EAuthProviders } from '../../EAuth';
import { Alerts } from '../../alerts';
import firebase from 'firebase';
import { User } from '../../User';

/**
 * Generated class for the UserLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html'
})
export class UserLoginPage {
  @ViewChild('username') uname;
  @ViewChild('password') password;

  user: User;

  public static DEFAULT_PHOTO_URL: string =
    'https://firebasestorage.googleapis.com/v0/b/test-project-54f8d.appspot.com/o/default.png?alt=media&token=3feb184a-2cf4-41a9-a044-1c5912434ed3';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private fire: AngularFireAuth,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserLoginPage');
  }

  login() {
    this.fire.auth
      .signInWithEmailAndPassword(this.uname.value, this.password.value)
      .then(data => {
        console.log('data: ', data);

        this.user = {
          loggedin: true,
          name: data.displayName,
          profilePicture: data.photoURL,
          email: this.uname.value,
          provider: EAuthProviders.local
        };

        const ref = firebase.storage().ref(this.user.email + '.png');
        ref
          .getDownloadURL()
          .then(u => {
            this.user.profilePicture = u;
            this.saveAndLogin();
          })
          .catch(e => {
            this.user.profilePicture = UserLoginPage.DEFAULT_PHOTO_URL;

            let fireUser = this.fire.auth.currentUser;
            fireUser
              .updateProfile({
                displayName: fireUser.displayName,
                photoURL: this.user.profilePicture
              })
              .then(() => {
                // then do nothing bro
              })
              .catch(e => console.error(e));

            this.saveAndLogin();
          });
      })
      .catch(error => {
        console.error('error: ', error);
        Alerts.alert('Error', error.message, this.alertCtrl);
      });
  }

  private saveAndLogin() {
    console.log(this.user);

    this.storage
      .set('user', JSON.stringify(this.user))
      .then(() => {
        this.navCtrl.push(TabsPage);
      })
      .catch(e => console.error(e));
  }

  showRegister() {
    this.navCtrl.push(RegisterPage).catch(e => console.error(e));
  }
}
