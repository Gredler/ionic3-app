import { EAuthProviders } from './../../EAuth';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import firebase from 'firebase';
import { User } from '../../User';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  providers = EAuthProviders;

  user: User = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    public navCtrl: NavController,
    private fire: AngularFireAuth,
    private storage: Storage
  ) {
    this.updateUser();
  }

  // update this.user when this page is about to become the active page!
  ionViewWillEnter() {
    this.updateUser();
  }

  private updateUser() {
    this.storage
      .get('user')
      .then(val => {
        this.user = JSON.parse(val);
        console.log(this.fire.auth.currentUser);
      })
      .catch(e => console.error(e));
  }

  // logout() {
  //   console.log(this.fire.auth.currentUser);

  //   this.fire.auth.signOut();

  //   this.storage.clear();

  //   const root = this.getNav();
  //   root.popToRoot ();

  //   // manually run a cicle for the component
  //   this.detectorRef.detectChanges();
  // }

  // private getNav(): any {
  //   var navs = this.app.getRootNavs();
  //   if (navs && navs.length > 0) {
  //     return navs[0];
  //   }
  //   return this.app.getActiveNav();
  // }
}
