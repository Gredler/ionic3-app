import { ChangePasswordPage } from './../change-password/change-password';
import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  ActionSheetController,
  ToastController
} from 'ionic-angular';
import { EAuthProviders } from '../../EAuth';
import { User } from '../../User';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';
import { AngularFireStorage } from 'angularfire2/storage';
import { UserLoginPage } from '../user-login/user-login';

/**
 * Generated class for the OptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html'
})
export class OptionsPage {
  @ViewChild('username') uname;

  progress: Observable<number>; // Observable 0 to 100
  image: string; // base64
  downloadURL;

  private filePath;

  user: User = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fire: AngularFireAuth,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public toastCtrl: ToastController,
    public fireStorage: AngularFireStorage,
    private storage: Storage,
    private app: App
  ) {
    this.storage
      .get('user')
      .then(val => {
        this.user = JSON.parse(val);
      })
      .catch(e => console.error(e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OptionsPage');
  }

  submitData() {
    console.log('submitting');

    this.user.name = this.uname.value;

    let fireUser = this.fire.auth.currentUser;
    fireUser
      .updateProfile({
        displayName: this.user.name,
        photoURL: this.user.profilePicture
      })
      .then(() => {
        this.storage
          .set('user', JSON.stringify(this.user))
          .then(() => {
            this.app.getRootNav;
            this.app
              .getRootNav()
              .getActiveChildNav()
              .select(0);
          })
          .catch(e => console.error(e));
      })
      .catch(e => console.error(e));
  }

  showChangePassword() {
    this.navCtrl.push(ChangePasswordPage).catch(e => console.error(e));
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.handleAction(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.handleAction(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Revert to default',
          handler: () => {
            this.revertToDefault();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present().catch(e => console.error(e));
  }

  private async handleAction(sourceType: PictureSourceType) {
    let image = await this.getPicture(sourceType);
    this.createUploadTask(image);
  }

  private createUploadTask(file: string) {
    this.filePath = `${this.user.email}.png`;

    this.image = 'data:image/png;base64,' + file;

    const ref = this.fireStorage.ref(this.filePath);
    const task = ref.putString(this.image, 'data_url');

    this.progress = task.percentageChanges();

    task
      .then(() => this.uploadFinishedHandler())
      .catch(e => console.error(e));
  }

  public async getPicture(sourceType) {
    // Create options for the Camera Dialog
    var options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL, //base64
      encodingType: this.camera.EncodingType.PNG
    };

    return await this.camera.getPicture(options).catch(e => console.error(e));
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present().catch(e => console.error(e));
  }

  private uploadFinishedHandler() {
    const ref = firebase.storage().ref(this.filePath);
    ref
      .getDownloadURL()
      .then(res => {
        this.downloadURL = res;
        this.user.profilePicture = this.downloadURL;
      })
      .catch(e => console.error(e));

    this.progress = null;
      this.presentToast('Upload successfull!');
  }

  private revertToDefault() {
    this.downloadURL = UserLoginPage.DEFAULT_PHOTO_URL;
    this.user.profilePicture = this.downloadURL;
    this.filePath = null;
    this.image = null;

    const ref = firebase.storage().ref(this.user.email + '.png');

    ref
      .delete()
      .then(() => this.saveToStorage())
      .catch(e => console.error(e));
  }

  private saveToStorage() {
    this.storage
    .set('user', JSON.stringify(this.user))
    .then(() => {
      let fireUser = this.fire.auth.currentUser;
      fireUser
        .updateProfile({
          displayName: this.user.name,
          photoURL: this.user.profilePicture
        })
        .catch(e => console.error(e));
    })
    .catch(e => console.error(e));
  }
}
