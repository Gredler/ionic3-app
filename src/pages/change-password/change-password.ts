import { Alerts } from './../../alerts';
import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import { User } from '../../User';
import { EAuthProviders } from '../../EAuth';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html'
})
export class ChangePasswordPage {
  @ViewChild('current') currentPW;
  @ViewChild('new1') newPW;
  @ViewChild('new2') newPWConfirm;

  user: User = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fire: AngularFireAuth,
    private storage: Storage,
    private alertCtrl: AlertController
  ) {
    this.storage
      .get('user')
      .then(val => {
        this.user = JSON.parse(val);
      })
      .catch(e => console.error(e));
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  changePassword() {
    console.log('change password');

    const credential = firebase.auth.EmailAuthProvider.credential(
      this.user.email,
      this.currentPW.value
    );
    this.fire.auth.currentUser
      .reauthenticateWithCredential(credential)
      .then(val => {
        if (this.newPW.value === this.newPWConfirm.value) {
          if (this.newPW.value !== this.currentPW.value) {
            this.fire.auth.currentUser
              .updatePassword(this.newPW.value)
              .then(() => {
                this.navCtrl.pop().catch(e => console.error(e));
              })
              .catch(e => {
                Alerts.alert(e.code, e.message, this.alertCtrl);
              });
          } else {
            Alerts.alert(
              "Old Password",
              "You can't use your current Password as your new Password!",
              this.alertCtrl
            );
          }
        } else {
          Alerts.alert(
            "Passwords don't match",
            "The passwords you entered don't match!",
            this.alertCtrl
          );
        }
      })
      .catch(e => {
        console.error(e);
        Alerts.alert(
          'Wrong Password',
          'The password you entered is wrong!',
          this.alertCtrl
        );
      });
  }
}
