import { User } from './../../User';
import { UserLoginPage } from './../user-login/user-login';
import { TabsPage } from './../tabs/tabs';
import { EAuthProviders } from './../../EAuth';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  providers = EAuthProviders;

  user: User = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    public navCtrl: NavController,
    private fire: AngularFireAuth,
    private detectorRef: ChangeDetectorRef,
    private storage: Storage
  ) {
    // Push the TabsPage if the user is already logged in
    this.storage
      .get('user')
      .then(val => {
        if (val !== null && val !== undefined) {
          this.navCtrl.push(TabsPage);
        }
      })
      .catch(e => console.error(e));
  }

  login(authProvider: EAuthProviders) {
    // this.fire.auth
    //   .signInWithPopup(this.getProvider(authProvider))
    //   .then(res => {
    this.fire.auth
      .signInWithRedirect(this.getProvider(authProvider))
      .then(() => {
        this.fire.auth
          .getRedirectResult()
          .then(res => {
            //         console.log(res);

            this.user.loggedin = true;
            this.user.name = res.user.displayName;
            this.user.email = res.user.email;
            this.user.profilePicture = res.user.photoURL;
            this.user.provider = authProvider;

            // Get high quality profile pictures for facebook and twitter
            if (this.user.provider === EAuthProviders.facebook) {
              this.user.profilePicture += '?height=500';
            } else if (this.user.provider === EAuthProviders.twitter) {
              this.user.profilePicture = this.user.profilePicture.replace(
                'normal',
                '400x400'
              );
              console.log(this.user.profilePicture);
            }

            this.storage
              .set('user', JSON.stringify(this.user))
              .then(() => {
                this.navCtrl.push(TabsPage);
                // manually run a cycle for the component
                this.detectorRef.detectChanges();
              })
              .catch(e => console.error(e));
          })
          .catch(exc => console.error(exc));
      })
      .catch(e => console.error(e));
  }

  private getProvider(provider: EAuthProviders) {
    let retProvider;

    switch (provider) {
      case EAuthProviders.facebook:
        retProvider = new firebase.auth.FacebookAuthProvider();
        break;
      case EAuthProviders.google:
        retProvider = new firebase.auth.GoogleAuthProvider();
        break;
      case EAuthProviders.twitter:
        retProvider = new firebase.auth.TwitterAuthProvider();
        break;
      case EAuthProviders.github:
        retProvider = new firebase.auth.GithubAuthProvider();
        break;
      default:
        retProvider = new firebase.auth.FacebookAuthProvider();
        break;
    }

    return retProvider;
  }

  showUserLogin() {
    this.navCtrl.push(UserLoginPage).catch(e => console.error(e));
  }
}
