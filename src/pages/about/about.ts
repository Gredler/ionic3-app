import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
} from 'ionic-angular';

import { User } from '../../User';
import { EAuthProviders } from '../../EAuth';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  user: User = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private fire: AngularFireAuth
  ) {
    this.storage
      .get('user')
      .then(val => {
        this.user = JSON.parse(val);
      })
      .catch(e => console.error(e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
}
