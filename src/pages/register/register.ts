import { Alerts } from './../../alerts';
import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  @ViewChild('username') uname;
  @ViewChild('password1') password1;
  @ViewChild('password2') password2;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fire: AngularFireAuth,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  register() {
    if (this.password1.value === this.password2.value) {
      this.fire.auth
        .createUserWithEmailAndPassword(this.uname.value, this.password1.value)
        .then(data => {
          console.log('got data: ', data);
          Alerts.alertWithPop('Success!', "You're registered", this.alertCtrl, this.navCtrl);
        })
        .catch(error => {
          console.error('error: ', error);
          Alerts.alert('Error!', error.message, this.alertCtrl);
        });
      console.log(
        'Registering User ' + this.uname.value + ' - ' + this.password1.value
      );
    } else {
      Alerts.alert('Error', 'The passwords do not match!', this.alertCtrl);
    }
  }
}
