import { EAuthProviders } from "./EAuth";

export class User {
    loggedin: boolean
    name: string
    profilePicture: string
    email: string
    provider: EAuthProviders
}
